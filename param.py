

import numpy as np

# Assign TB parameters for 'material' to a dictionary
# Nmaterials = all_parameters[:,0].shape
# Nparameters = all_parameters[0,:].shape
# Read in in eV and returned in eV

def load_tb_parameters(material):
    material_order = {'c':0,'si':1,'ge':2}
    all_parameters = np.loadtxt('TBparameters.dat')
    oneset = all_parameters[material_order[material],:]
    parameters = {'Esa': oneset[0],
                  'Epa': oneset[1],
                  'Esc': oneset[2],
                  'Epc': oneset[3],
                  'Essta': oneset[4],
                  'Esstc': oneset[5],

                  'Vss': oneset[6],
                  'Vxx': oneset[7],
                  'Vxy': oneset[8],
                  'Vsa_pc'  : oneset[9],
                  'Vsc_pa'  : oneset[10],
                  'Vssta_pc': oneset[11],
                  'Vpa_sstc': oneset[12]}
    return parameters

