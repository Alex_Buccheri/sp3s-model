
#Modules
import numpy as np
import sys

#My modules
from constants import i_img



# Sum of exponential phase factors
def g0(k,dvec):
    d1 = dvec[0]
    d2 = dvec[1]
    d3 = dvec[2]
    d4 = dvec[3]
    gfactor = np.exp(i_img * np.dot(d1, k)) + np.exp(i_img * np.dot(d2, k)) + np.exp(i_img * np.dot(d3, k)) + np.exp(i_img * np.dot(d4, k))
    return 0.25*gfactor

def g1(k,dvec):
    d1 = dvec[0]
    d2 = dvec[1]
    d3 = dvec[2]
    d4 = dvec[3]
    gfactor = np.exp(i_img * np.dot(d1, k)) + np.exp(i_img * np.dot(d2, k)) - np.exp(i_img * np.dot(d3, k)) - np.exp(i_img * np.dot(d4, k))
    return 0.25 * gfactor

def g2(k,dvec):
    d1 = dvec[0]
    d2 = dvec[1]
    d3 = dvec[2]
    d4 = dvec[3]
    gfactor = np.exp(i_img * np.dot(d1, k)) - np.exp(i_img * np.dot(d2, k)) + np.exp(i_img * np.dot(d3, k)) - np.exp(i_img * np.dot(d4, k))
    return 0.25 * gfactor

def g3(k,dvec):
    d1 = dvec[0]
    d2 = dvec[1]
    d3 = dvec[2]
    d4 = dvec[3]
    gfactor = np.exp(i_img * np.dot(d1, k)) - np.exp(i_img * np.dot(d2, k)) - np.exp(i_img * np.dot(d3, k)) + np.exp(i_img * np.dot(d4, k))
    return 0.25 * gfactor


# Bulk Hamiltonian for zincblende systems in sp3s* basis
def construct_bulk_H(TBparameters,k,d):

    # Assign TB parameters
    Esa   =  TBparameters['Esa']
    Epa   =  TBparameters['Epa']
    Essta =  TBparameters['Essta']
    Esc   =  TBparameters['Esc']
    Epc   =  TBparameters['Epc']
    Esstc =  TBparameters['Esstc']

    Vss    = TBparameters['Vss']
    Vsa_pc = TBparameters['Vsa_pc']
    Vpa_sc = TBparameters['Vsc_pa']  #Private comm with Vogl suggests the parameter label in T.1 is a typo. I've used it here anyway
    Vxx    = TBparameters['Vxx']
    Vxy    = TBparameters['Vxy']
    Vssta_pc = TBparameters['Vssta_pc']
    Vpa_sstc = TBparameters['Vpa_sstc']

    # Define Haa, Hac, Hca and Hcc
    # Diagonal blocks
    N = 5
    Haa = np.zeros(shape=(N,N), dtype=complex)
    Hcc = np.zeros(shape=(N,N), dtype=complex)
    row,col = np.diag_indices(Haa.shape[0])
    Haa[row,col] = [Esa,Epa,Epa,Epa,Essta]
    Hcc[row,col] = [Esc,Epc,Epc,Epc,Esstc]

    # Inter-atomic block
    Hac = np.array( [[    Vss*g0(k,d), Vsa_pc*g1(k,d),   Vsa_pc*g2(k,d),   Vsa_pc*g3(k,d),   0.            ],
                     [-Vpa_sc*g1(k,d),    Vxx*g0(k,d),      Vxy*g3(k,d),      Vxy*g2(k,d),  -Vpa_sstc*g1(k,d)],
                     [-Vpa_sc*g2(k,d),    Vxy*g3(k,d),      Vxx*g0(k,d),      Vxy*g1(k,d),  -Vpa_sstc*g2(k,d)],
                     [-Vpa_sc*g3(k,d),    Vxy*g2(k,d),      Vxy*g1(k,d),      Vxx*g0(k,d),  -Vpa_sstc*g3(k,d)],
                     [ 0.,           Vssta_pc*g1(k,d), Vssta_pc*g2(k,d), Vssta_pc*g3(k,d),   0.            ]])


    H = np.zeros(shape=(N*2, N*2), dtype=complex)
    H[0:N, 0:N]     = Haa
    H[N:2*N, N:2*N] = Hcc
    H[0:N, N:2*N]   = Hac
    H[N:2*N, 0:N]   = np.conjugate( Hac.transpose() )

    return H



