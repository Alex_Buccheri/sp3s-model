
# -----------------------------------------------
# sp3s* Tight-Binding Model
# Based on DOI: ﻿10.1016/0022-3697(83)90064-1
# -----------------------------------------------

#Modules
import sys
import numpy as np
from scipy import linalg

#My modules
import ham
import param
import constants
#From entos testing code
import bands as bs


# Build 2 atom structure - no need, just specify material
material='si'

# Read in TB parameters for material
TBparameters = param.load_tb_parameters(material)

# Build and solve 2-atom Hamiltonian at k=gamma
dvec = constants.displacement_vectors(material)
kpoint = [0.,0.,0.]
H = ham.construct_bulk_H(TBparameters, kpoint, dvec)
#print(np.real(H))

# Look consistent with values printed in T.2 of paper
w = linalg.eigh(H, lower=True, eigvals_only=True)
print('Eigenvalues of',material,'at gamma (in eV):')
print(w)
print('')

# Sample k-points along k-vectors linking high symmetry points
bz_path = ['L-T','T-X']
ksampling = [20,20]
kpoints = bs.generate_band_structure_kpoints(bz_path,ksampling,material)
#print(kpoints)

eigen = []
for kpoint in kpoints:
    H = ham.construct_bulk_H(TBparameters, kpoint, dvec)
    w = linalg.eigh(H, lower=True, eigvals_only=True)
    eigen.append(w)

# Plot band structure for 2-atom Hamiltonian
bs.export_bandstructure_data(bz_path,ksampling,material,eigen)