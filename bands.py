#!/usr/bin/env python3

#Modules
import numpy as np

#My modules
import constants

# -----------------------------
# Generate band structure
# -----------------------------

def displacement_vectors(bz_path):
    dvector = []
    for path in bz_path:
        start,stop = path.split('-')
        k1 = constants.BZ.symmetry_point[start]
        k2 = constants.BZ.symmetry_point[stop]
        dvector.append(k2-k1)
    return dvector


# For a path through the Brillouin zone, compute discrete k-points
# in units of 2pi/a
# Pass bz_path = ['L-T','T-X'] and ksampling = [10,10], for example
def generate_band_structure_kpoints(bz_path,ksampling,material):
    kpoint = []
    unit = (2.*np.pi/constants.lattice_constant(material))
    for ivec in range(0,len(bz_path)):
        start, stop = bz_path[ivec].split('-')
        k1 = constants.BZ.symmetry_point[start]*unit
        k2 = constants.BZ.symmetry_point[stop]*unit
        dvec = k2-k1
        nk = ksampling[ivec]
        dk = dvec/nk
        for ik in range(0,nk):
            kpoint.append(k1 + ik*dk)
    last_point = ''.join(bz_path)[-1:]
    kpoint.append(constants.BZ.symmetry_point[last_point]*unit)
    return kpoint

#Export band structure in eV
def export_bandstructure_data(bz_path,ksampling,material,eigen):
    kpoint = 0.
    unit = (2. * np.pi / constants.lattice_constant(material))
    fid = open(file=material+'_bs.dat', mode='w')
    for ivec in range(0,len(bz_path)):
        start, stop = bz_path[ivec].split('-')
        k1 = constants.BZ.symmetry_point[start]*unit
        k2 = constants.BZ.symmetry_point[stop]*unit
        knorm  = np.linalg.norm(k2-k1)
        Nk = ksampling[ivec]
        for ik in range(0,Nk):
            ik_global = ik + (ivec*Nk)
            kpoint += ik*(knorm/float(Nk-1))
            print(ik_global, *eigen[ik_global], sep=" ",file=fid)
    fid.close()
    return









