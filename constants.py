
import numpy as np

# ---------------------
# Constants
# ---------------------
i_img = complex(0,1)  #Equally valid i_img = 0.+1.j

# In angstrom
bond_length = {'si':2.35}

#Unity Displacement vectors of NN, for anion-centre
unity_d1 = [ 1., 1., 1.]
unity_d2 = [ 1.,-1.,-1.]
unity_d3 = [-1., 1.,-1.]
unity_d4 = [-1.,-1., 1.]

def lattice_constant(material):
    return (4./(3.**0.5))*bond_length[material]

def displacement_vectors(material):
    al = lattice_constant(material)
    d1 = [0.25 * al * i for i in unity_d1]
    d2 = [0.25 * al * i for i in unity_d2]
    d3 = [0.25 * al * i for i in unity_d3]
    d4 = [0.25 * al * i for i in unity_d4]
    return [d1,d2,d3,d4]

class BZ:
    #Private
    # High symmetry points in units of 2 pi/al (reduced crystalographic)
    _X=np.array([0.,1.,0.])
    _T=np.array([0.,0.,0.])
    _L=np.array([0.5,0.5,0.5])
    _U=np.array([0.25,1.,0.25])
    _K=np.array([0.75,0.75,0.])
    _W=np.array([0.5,1.,0.])
    #Public
    k_units='reduced'
    symmetry_point={'X':_X,'T':_T,'L':_L,'U':_U,'K':_K,'W':_W}