# README #

### Python implementation of Vogl et. al.'s sp^3sp^* model for bulk semiconductors ###

* [A Semi-empirical tight-binding theory of the electronic structure of semiconductors†](https://doi.org/10.1016/0022-3697(83)90064-1)
* DOI: 10.1016/0022-3697(83)90064-1


### Details ###

* Python 3.6, numpy 
* Only for bulk systems in the absence of spin-orbit coupling: Not generalised to N atoms 
* Errors in Hamiltonian and parameters published in original paper are corrected 
* Not all lattice constants have been copied into the code 
* Alexander Buccheri: ab17369@bristol.ac.uk
